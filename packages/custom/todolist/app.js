'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Todolist = new Module('todolist');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Todolist.register(function(app, auth, database, circles) {

  //We enable routing. By default the Package Object is passed to the routes
  Todolist.routes(app, auth, database, circles);

  //We are adding a link to the main menu for all authenticated users
  Todolist.menus.add({
    title: 'TodoList',
    link: 'todo_list',
    roles: ['all'],
    menu: 'main'
  });

  return Todolist;
});
